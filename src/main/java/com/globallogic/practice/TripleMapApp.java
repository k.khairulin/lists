package com.globallogic.practice;

public class TripleMapApp {
	
	public static void main(String[] args) {
		TripleMap<Integer, String, String> tp = new TripleMap<>();
		tp.put(1, "one", "uno");
		tp.put(2, "two", "dos");
		tp.put(4, "four", "cuatro");
		System.out.println(tp.size());
		System.out.println(tp.getFirstValue(2));
		System.out.println(tp.getSecondValue(4));
		tp.remove(2);
		System.out.println(tp.size());
	}
}
