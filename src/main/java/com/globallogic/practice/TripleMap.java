package com.globallogic.practice;

import java.util.HashMap;
import java.util.Map;

public class TripleMap<K, V1, V2> {
	
	private Map<K, Pair> map;

	public TripleMap() {
		map = new HashMap<>();
	}

	private class Pair {
		private V1 pairValue1;
		private V2 pairValue2;
		
		private Pair(V1 pairValue1, V2 pairValue2) {
			this.pairValue1 = pairValue1;
			this.pairValue2 = pairValue2;
		}
		
		public V1 getPairValue1() {
			return pairValue1;
		}
		
		public V2 getPairValue2() {
			return pairValue2;
		}	
	}
	
	public void put(K key, V1 value1, V2 value2) {
		map.put(key, new Pair(value1, value2));
	}
	
	public int size() {
		return map.size();
	}
	
	public V1 getFirstValue(K key) {
		return map.get(key).getPairValue1();
	}
	
	public V2 getSecondValue(K key) {
		return map.get(key).getPairValue2();
	}
	
	public void remove(K key) {
		map.remove(key);
	}
}
