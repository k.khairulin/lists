package com.globallogic.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Sorter {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Specify list length:");
		int length = in.nextInt();
		List<Integer> inputList = new ArrayList<>(length);
		System.out.println("Specify list values:");
		for (int i = 0; i < length; i++) {
			inputList.add(in.nextInt());
		}
		inputList.sort((a1, a2) -> a2 - a1);
		inputList.forEach(System.out::println);
	}
}
