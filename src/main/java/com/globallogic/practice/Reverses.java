package com.globallogic.practice;

import java.util.Scanner;

public class Reverses {
	public static final int maxValue = 100000;
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Specify array length:");
		int inputLength = in.nextInt();
		int[] intArray = new int[maxValue];
		System.out.println("Specify values:");
		for (int i = 0; i < inputLength; i++) {
			int currentValue = in.nextInt();
			intArray[currentValue] = 1;
		}
		for (int i = 0; i < intArray.length; i++) {
			if (intArray[i] > 0) {
				System.out.println(i);
			}
		}
	}
}
